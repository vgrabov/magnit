from config import BASE_DIR, DATABASE_NAME
import sqlite3
import os


def exec_sql(script):
    f = open(os.path.join(BASE_DIR, script))
    sql = f.read()

    con = sqlite3.connect(os.path.join(BASE_DIR, DATABASE_NAME))
    cur = con.cursor()

    cur.executescript(sql)
    con.commit()

exec_sql('scheme.sql')
exec_sql('fixtures.sql')