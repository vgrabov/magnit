import sys
import os
#  Need add project dir to sys.path for WSGI server.
BASE_DIR = os.path.abspath(os.path.dirname(__file__))
sys.path.insert(0, BASE_DIR)

from wsgiref.simple_server import make_server
from core.http.request import Request
from core.http.response import Response404, Response500
from core.routing import resolve


def app(environ, start_response):

    try:
        request = Request(environ)          # Parse env in request
        controller = resolve(request)       # Get action for this request

        if controller:
            response = controller(request)  # Do action
        else:
            response = Response404()        # Controller not found. Display 404 error.

        return response(start_response)

    except:
        # If have errors while run. Display error(Traceback if DEBUG true)
        return Response500()(start_response)



application = app

if __name__ == '__main__':
    print "Run server http://localhost:8000"
    server = make_server('localhost', 8000, application)
    server.serve_forever()