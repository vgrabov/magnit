import re
from core.orm.base import BaseModel


class Region(BaseModel):
    table_name = 'region'
    fields = ['id', 'title', ]


class City(BaseModel):
    table_name = 'city'
    fields = ['id', 'region', 'title',]


class Comment(BaseModel):
    table_name = 'comment'
    fields = ['id', 'fam', 'name', 'otch', 'city', 'phone', 'email', 'text']

    def is_valid(self):

        if self.values.get('name', '') == '':
            self.errors.append('name')

        if self.values.get('fam', '') == '':
            self.errors.append('fam')

        if self.values.get('text', '') == '':
            self.errors.append('text')

        if self.values.get('phone') and not \
                (re.match(r'^\(\d{3,5}\) \d{5,7}$', self.values.get('phone', '')) and \
                         len(self.values['phone']) != 15):
            self.errors.append('phone')

        if self.values.get('email') and not \
                re.match('^[^@]+@[^@]+\.[^@]+$', self.values.get('email', '')):
            self.errors.append('email')

        if self.errors:
            return False

        return True