#-*- coding: utf-8 -*-
import json
from core.http.response import Response
from core.orm.query import SelectQuery
from models import Region, City


def ajax_region(request):
    """
    List of all regions
    return: list in json format.
    """
    region_list = Region.all()
    return Response(json.dumps(region_list).decode('unicode_escape'), content_type='application/json')


def ajax_city(request):
    """
    List of cities.
    """
    query = SelectQuery(City)

    region = request.GET.get('region')

    # if pass region filter cities in this region.
    if region:
        try:
            region = int(region)
        except:
            return Response('Error: Region must be integer.')
        else:
            query.filter('region=%d' % region)

    res = query.do().fetchall()
    return Response(json.dumps(res).decode('unicode_escape'), content_type='application/json')
