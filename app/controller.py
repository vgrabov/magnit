#-*- coding: utf-8 -*-

from core.http.response import Response, ResponseRedirect
from core.template import Template
from core.orm.query import connection
from models import Region, Comment, City


def index(request):
    return ResponseRedirect('/comment/')


def comment(request):
    template = Template.load_from_file('comment.html')

    if request.method == 'POST':
        com = Comment(request.POST)
        if com.is_valid():
            com.save()
            return Response(template.render({'message': 'Ваш комментарий успешно добавлен'}))

        context = {'message': 'Исправьте ошибки ниже', 'errors': ','.join(com.errors)}
        context.update(request.POST)        # Add comment fields to context for init inputs.

        return Response(template.render(context))

    return Response(template.render({}))


def view(request):
    template = Template.load_from_file('comment_list.html')
    comments = Comment.all()
    rows = []

    for c in comments:
        comment_template = """
        <p>ФИО: ${fam} ${name} ${otch}</p>
        <p>Город: ${region} ${city}</p>
        <p>Контакты: ${phone} ${email}</p>
        <p>Текст: ${text}</p>
        <p><a href="/comment/delete/${id}">Удалить</a>
        """
        if c.get('city'):
            city = City.filter(where=['id=%d' % c['city']])[0]
            region = Region.filter(where=['id=%d' % city['region']])[0]
            c['city'] = city['title']
            c['region'] = region['title']

        rows.append(Template(comment_template).render(c))
    return Response(template.render({
        'rows': '<hr>'.join(rows)
    }))


def delete_comment(request):
    comment_id = int(request.path.split('/')[-1])
    Comment.delete(comment_id)
    return ResponseRedirect('/view/')


def stat(request):
    template = Template.load_from_file('stat.html')
    rows = []
    row_template = """
        <tr>
            <td><a href="/stat/${id}/">${title}</a></td>
            <td>${count}</td>
        </tr>
    """
    query = """
        SELECT
          region.id, region.title, COUNT(comment.id) as count
        FROM
          region, comment, city
        WHERE
          city.region = region.id AND comment.city = city.id
        GROUP BY
          region.id
        HAVING
            COUNT(comment.id) > 5
        ORDER BY
          count DESC
        """
    res = connection.execute(query).fetchall()
    for reg in res:
        rows.append(Template(row_template).render(reg))

    return Response(template.render({
        'rows': "".join(rows),
    }))


def stat_region(request):
    region_id = int(request.path.split('/')[-2])
    template = Template.load_from_file('stat_region.html')
    rows = []
    row_template = """
        <tr>
            <td>${title}</td>
            <td>${count}</td>
        </tr>
    """
    query = """
        SELECT
          region.id, city.title, COUNT(comment.id) as count
        FROM
          region, comment, city
        WHERE
          city.region = region.id AND comment.city = city.id AND region.id = %d
        GROUP BY
          city.id
        ORDER BY
          count DESC
        """ % region_id
    res = connection.execute(query).fetchall()

    for reg in res:
        rows.append(Template(row_template).render(reg))

    return Response(template.render({
        'rows': "".join(rows),
    }))