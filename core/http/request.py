from urlparse import parse_qs


class Request(object):
    """
    WSGI environ wrapper
    """
    def __init__(self, environ):
        """
        Parse environ.
        :param environ: env from WSGI server
        """
        try:
            self.path = environ['PATH_INFO']
            self.method = environ['REQUEST_METHOD'].upper()

            if environ.get('CONTENT_LENGTH', '') == '':
                self.content_length = 0
            else:
                self.content_length = int(environ.get('CONTENT_LENGTH'))

            #  Parse GET and POST
            self.GET = self.parse_query(environ['QUERY_STRING'])

            if self.method == 'POST':
                self.POST = self.parse_query(environ['wsgi.input'].read(self.content_length))

        except KeyError:
            #  BAD REQUEST
            raise

    def parse_query(self, qs):
        data = parse_qs(qs)
        res = {}

        for key in data:
            res[key] = data[key][0]

        return res