import sys
import types
from traceback import format_tb
from httplib import responses as REASON_PHRASES
import config


class BaseResponse(object):
    status_code = 200
    content_type = 'text/html'
    data = ''

    def __init__(self, data=None, status=None, content_type=None):
        if content_type is not None:
            self.content_type = content_type

        if status is not None:
            self.status_code = status

        if data is not None:
            self.data = data

    def get_status(self):
        return "%d %s" % (self.status_code, REASON_PHRASES[self.status_code])

    def get_headers(self):
        return [
            ('Content-Type', self.content_type),
            ('Cache-Control', 'no-cache'),
        ]

    def get_data(self):
        #  Override this method if the response should return the data
        return []

    def __call__(self, start_response):
        start_response(self.get_status(), self.get_headers())
        data = self.get_data()

        if data is not None:
            return data


class Response(BaseResponse):

    def get_data(self):
        if isinstance(self.data, types.UnicodeType):
            self.data = self.data.encode('utf-8')
        return [self.data]


class ResponseRedirect(BaseResponse):
    status_code = 301

    def __init__(self, url, *args, **kwargs):
        super(ResponseRedirect, self).__init__(*args, **kwargs)
        self.redirect_url = url

    def get_headers(self):
        headers = [
            ('Location', self.redirect_url),
            ('Cache-Control', 'no-cache'),
        ]
        return headers


class Response404(BaseResponse):
    status_code = 404

    def get_data(self):
        return ['<h1>404</h1> Page not found']


class Response500(BaseResponse):
    status_code = 500

    def get_data(self):
        if config.DEBUG:
            #  If debug mode, show traceback
            e_type, e_value, tb = sys.exc_info()
            traceback = ['<h1>500</h1> Traceback (most recent call last):']
            traceback += format_tb(tb)
            traceback.append('%s: %s' % (e_type.__name__, e_value))
            data = '<br>'.join(traceback)
        else:
            #  Or 500 error
            data = "<h1>500</h1> Internal server error."

        return [data]

    def __call__(self, start_response):
        start_response(self.get_status(), self.get_headers(), sys.exc_info())
        data = self.get_data()

        if data is not None:
            return data