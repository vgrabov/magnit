import re
from urls import urls


def resolve(request):
    """
        Parse urls.py and find view for this request
        :param request:
        :return: callback action or None if not found.

    """
    path = request.path

    for regex, callback in urls:
        match = re.search(regex, path)
        if match is not None:
            return callback