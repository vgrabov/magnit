import os
from mimetypes import guess_type
from core.http.response import Response, Response404
from config import STATIC_DIR


def get_static_file(request):
    """
    Return static file.
    :param request:
    :return: Response object with file data and mimetype.
    """
    path = request.path.split('/')[2:]
    path = os.path.join(STATIC_DIR, *path)

    if path and os.path.isfile(path):
        f = open(path, 'r')
    else:
        return Response404()

    content_type = guess_type(path)[0]
    return Response(f.read(), content_type=content_type)