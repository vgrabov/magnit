import os
import re
import config
import string


class Template(object):
    _body = ''

    def __init__(self, str):
        self._body = str

    @classmethod
    def load_from_file(cls, path):
        abs_path = os.path.join(config.TEMPLATES_DIR, path)

        if os.path.isfile(abs_path):
            f = open(abs_path, 'r')
        else:
            raise OSError('File %s does not exist' % abs_path)
        template = cls(f.read())
        return template

    def render(self, context):
        import types
        cont = {}
        for key in context:     # Parse context encode to utf-8 and delete None items
            if isinstance(context[key], types.UnicodeType):
                context[key] = context[key].encode('utf-8')
            if context[key] is not None:
                cont[key] = context[key]
        text = string.Template(self._body).safe_substitute(cont)

        #  Delete tags in template if they not found in context. Tag example ${variable}
        text = re.sub('\$\{\w[A-z0-9\.]+\}', '', text)
        return text