import sqlite3

from config import DATABASE_NAME
from core.orm.query import SelectQuery, InsertQuery, DeleteQuery


class BaseModel(object):
    table_name = ''
    fields = None

    def __init__(self, initial=None):
        self.values = {}
        self.errors = []
        if initial and isinstance(initial, dict):
            self.values = initial

    def is_valid(self):
        """
        Validate model. Override this method in current model.
        :return: True if model is valid.
        """
        return True


    @classmethod
    def all(cls):
        query = SelectQuery(cls)
        return query.do().fetchall()

    @classmethod
    def filter(cls, where):
        query = SelectQuery(cls, where=where)
        return query.do().fetchall()

    @classmethod
    def create(cls, values):
        query = InsertQuery(cls, values)
        return query.do()

    @classmethod
    def delete(cls, pk):
        query = DeleteQuery(cls, pk)
        return query.do()

    def save(self):
        query = InsertQuery(self, self.values)
        return query.do()