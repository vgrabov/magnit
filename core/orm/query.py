import sqlite3

from config import DATABASE_NAME


#  Wrap result of sql query to dict
def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

connection = sqlite3.connect(DATABASE_NAME)
connection.row_factory = dict_factory


class Query(object):
    query = ''
    _compiled = False
    _model = None

    def __init__(self, model):
        self._model = model

    # Execute query
    def do(self):
        if not self._compiled:
            self.compile()
        res = connection.execute(self.query)
        connection.commit()
        return res


class SelectQuery(Query):

    def __init__(self, model, fields=None, where=None, order_by=None):
        super(SelectQuery, self).__init__(model)

        self._fields = []
        self._where = []
        self._order_by = []

        if fields:
            if not isinstance(fields, list):
                self._fields = map(str.strip, fields.split(','))            #  Split by "," and strip items
            else:
                self._fields = fields

        if where:
            self.filter(where)

        if order_by:
            self.order_by(order_by)

    def filter(self, expression):
        """
        Add where in SQL query.
        :param clause: List or str example ['foo=1', 'bar=2'] or 'foo=1'
        """
        if isinstance(expression, list):
            self._where.extend(expression)
        else:
            self._where.append(expression)

    def order_by(self, param):
        if isinstance(param, list):
            self._order_by.extend(param)
        else:
            self._order_by.append(param)

    # Compile class to valid SQL query
    def compile(self):
        fields = ', '.join(self._fields) or '*'  # Join fields if exist or '*'
        self.query = "SELECT %s FROM %s" % (fields, self._model.table_name)

        if self._where:
            self.query = "%s WHERE %s" % (self.query, ' and '.join(self._where))

        if self._order_by:
            self.query = "%s ORDER BY %s" % (self.query, ', '.join(self._order_by))

        self._compiled = True


class InsertQuery(Query):

    def __init__(self, model, values):
        super(InsertQuery, self).__init__(model)

        if not isinstance(values, dict):
            raise TypeError

        fields = {}
        for key in values:
            if key in model.fields:
                fields[key] = values[key]

        self._values = fields

    def compile(self):
        fields = ','.join(self._values.keys())
        values = ', '.join(map(lambda x:'"%s"' % x, self._values.values()))             #  Wrap values \" and join
        self.query = "INSERT INTO %s(%s) VALUES (%s)" % (self._model.table_name, fields, values)

        self._compiled = True


class DeleteQuery(Query):

    def __init__(self, model, pk):
        super(DeleteQuery, self).__init__(model)
        self.pk = int(pk)

    def compile(self):
        self.query = "DELETE FROM %s WHERE id=%d" % (self._model.table_name, self.pk)
        self._compiled = True
