# Url scheme for project.

from app.controller import index, comment, delete_comment, view, stat, stat_region
from app.ajax import ajax_region, ajax_city
from core.static import get_static_file
from config import STATIC_URL

# File need contain urls tuple.
urls = (
    # Format:
    # ('url_regexp_pattern', callback_view),

    (r'^/$', index),
    (r'^/comment/$', comment),
    (r'^/comment/delete/\d+', delete_comment),
    (r'^/view/$', view),
    (r'^/stat/$', stat),
    (r'^/stat/\d+', stat_region),

    (r'/api/v1/region', ajax_region),
    (r'/api/v1/city', ajax_city),
    (STATIC_URL, get_static_file),
)