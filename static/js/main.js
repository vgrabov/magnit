function get(id){
    return document.getElementById(id);
}


function fill_select(id, option_list){
    var select = get(id);

    for (var i=0; i<option_list.length; i++){
        var option = document.createElement('option');
        option.value = option_list[i].id || option_list[i][0];
        option.innerHTML = option_list[i].title || option_list[i][1];
        select.appendChild(option);
    }
}

function clear_select(id) {
    var select = get(id);

    var length = select.options.length;
    for (var i = 0; i < length; i++) {
        select.options[0] = null;
    }
    select.appendChild(document.createElement('option'));
    select.selectedIndex = 0;
}


function encodeUrlQuery(data) {
   var ret = [];
   for (var d in data)
      ret.push(encodeURIComponent(d) + "=" + encodeURIComponent(data[d]));
   return ret.join("&");
}


function ajax(options) {
    var xmlhttp = new XMLHttpRequest();
    var url = options.url;

    xmlhttp.onreadystatechange = function(){
        if (xmlhttp.readyState==4) {
            options.callback(xmlhttp.responseText);
        }
    };

    if (options.data && options.method.toUpperCase() == 'POST'){
        url = url+'?'+encodeUrlQuery(options.data);
    }
    xmlhttp.open(options.method, url);
    if (options.method.toUpperCase() == 'POST'){
        xmlhttp.send(options.data[0]);
    } else {
        xmlhttp.send();
    }
}