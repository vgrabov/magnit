import os

DEBUG = True

BASE_DIR = os.path.abspath(os.path.dirname(__file__))

TEMPLATES_DIR = os.path.join(BASE_DIR, 'templates')

STATIC_URL = '/static/'

STATIC_DIR = os.path.join(BASE_DIR, 'static')

DATABASE_NAME = os.path.join(BASE_DIR, 'db.sqlite3')
